<?php
    include './class/usuarios.php';
    $usr = $_GET['usr'];
    $pass = $_GET['pass'];
    $usuario = new usuarios();
    $validacion = $usuario->validar($usr, $pass);

    echo json_encode(array(
        'id' => $usuario->getId($usr, $pass),
        'validacion' => $validacion
    ));
?>