<?php
    class formulario
    {
        private $con;
        function __construct()
        {
            include './conexion.php';
            $this->con = new conexion();
        }
        public function createConsecutivo($usrId){
            $consecutivo = 1;
            $queryConsecutivo = 'SELECT pro_id FROM proceso';
            $resultadoConsecutivo = $this->con->select($queryConsecutivo);
            foreach ($resultadoConsecutivo as $res) {                
                $consecutivoString = (string) $consecutivo;
                switch (strlen($consecutivoString)) {
                    case 1:
                        $consecutivoFinal = '0000000'.$consecutivo;
                        break;
                    case 2:
                        $consecutivoFinal = '000000'.$consecutivo;
                        break;
                    case 3:
                        $consecutivoFinal = '00000'.$consecutivo;
                        break;
                    case 4:
                        $consecutivoFinal = '0000'.$consecutivo;
                        break;
                    case 5:
                        $consecutivoFinal = '000'.$consecutivo;
                        break;
                    case 6:
                        $consecutivoFinal = '00'.$consecutivo;
                        break;
                    case 7:
                        $consecutivoFinal = '0'.$consecutivo;
                        break;
                    case 8:
                        $consecutivoFinal = $consecutivo;
                        break;
                }
                if ($res['pro_id'] == $consecutivoFinal) {
                    $consecutivo++;
                }
            }
            $consecutivoString = (string) $consecutivo;
                switch (strlen($consecutivoString)) {
                    case 1:
                        $consecutivoFinal = '0000000'.$consecutivo;
                        break;
                    case 2:
                        $consecutivoFinal = '000000'.$consecutivo;
                        break;
                    case 3:
                        $consecutivoFinal = '00000'.$consecutivo;
                        break;
                    case 4:
                        $consecutivoFinal = '0000'.$consecutivo;
                        break;
                    case 5:
                        $consecutivoFinal = '000'.$consecutivo;
                        break;
                    case 6:
                        $consecutivoFinal = '00'.$consecutivo;
                        break;
                    case 7:
                        $consecutivoFinal = '0'.$consecutivo;
                        break;
                    case 8:
                        $consecutivoFinal = $consecutivo;
                        break;
                }
            $query = 'INSERT INTO proceso (pro_id, pro_usr_id, pro_fecha) VALUES ("'.$consecutivoFinal.'", '.$usrId.', NOW());';
            $resultado = $this->con->select($query);
            return json_encode($consecutivoFinal);
        }
        public function getConsecutivo($usrId){
            $query = 'SELECT pro_id FROM proceso WHERE pro_usr_id = '.$usrId.' ORDER BY 1 DESC LIMIT 1';
            $resultado = $this->con->select($query);
            $res = mysqli_fetch_array($resultado);
            return $res['pro_id'];
        }
        public function insertarProceso($descripcion, $sede, $presupuesto){
            $consecutivo = $this->getConsecutivo(1);
            //echo $this->getConsecutivo(1);
            $query = 'UPDATE proceso SET pro_descripcion = "'.$descripcion.'", pro_sede = '.$sede.', pro_presupuesto = '.$presupuesto.', pro_estado = "A" WHERE pro_id = "'.$consecutivo.'"';
            $resultado = $this->con->select($query);
            return json_encode($resultado);
        }
    }
?>