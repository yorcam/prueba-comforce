<?php
    include 'conexion.php';
    $con = new conexion();
    $query = 'SELECT * FROM proceso';
    $resultado = $con->select($query);
    $data = array(
        "data" => array()
    );
    foreach ($resultado as $res) {
        $queryUsuarios = 'SELECT * FROM usuarios WHERE usr_id = '.$res['pro_usr_id'];
        $resultadoUsuarios = $con->select($queryUsuarios);
        $resUsuarios = mysqli_fetch_array($resultadoUsuarios);
        $sede = '';
        switch ($res['pro_sede']) {
            case 1:
                $sede = 'Bogotá';
                break;
            case 2:
                $sede = 'México';
                break;
            case 3:
                $sede = 'Perú';
                break;
        }
        $presupuestoUSD = $res['pro_presupuesto'] / 3000;
        array_push($data['data'], array(
            $resUsuarios['usr_id'],
            $res['pro_id'],
            $res['pro_descripcion'],
            $sede,
            $res['pro_presupuesto'],
            $presupuestoUSD,
            $res['pro_fecha']
        ));
    }
    echo json_encode($data);
?>