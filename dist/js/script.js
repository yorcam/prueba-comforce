const server = 'http://127.0.0.1/prueba/';
var usrData = Array();
$(document).ready(function () {
    var usrId = window.location.search.split('=')[1];
    $.ajax({
        url: server + 'dist/php/getData.php',
        data: {usrId:usrId},
        dataType: 'json',
        type: 'get',
        success: function(data){
            usrData = data;

        }
    })
    $('#confirmSolicitud').click(function(){
        $.ajax({
            url: server + 'dist/php/createConsecutivo.php',
            //data: {id:1},
            dataType: 'json',
            type: 'post',
            success: function(data){
                $('#consecutivo').text('#'+data);
            }
        });
    });
    $('#enviarProceso').submit(function(e){
        e.preventDefault();
        var descr = this.descripcion.value;
        var sede = this.sede.value;
        var presu = this.presupuesto.value;
        $.ajax({
            url: server + 'dist/php/insertarProceso.php',
            dataType: 'json',
            type: 'get',
            data:{
                d: descr,
                s: sede,
                p: presu
            },
            success: function(data){
                $('#enviarProceso').unbind('submit').submit();
            }
        });
    });
    //console.log(usrData.consecutivo);

    $('.sidebar-menu').tree();
    $('#dataTable').DataTable({
        'ajax': 'http://127.0.0.1/prueba/dist/php/getResumenData.php'
    });
    $('.select2').select2();
  });