$(document).ready(function () {
    $('#formLogin').submit(function(e){
        e.preventDefault();
        var usuario = this.usuario.value;
        var password = this.password.value;
        $.ajax({
            url: 'http://127.0.0.1/prueba/dist/php/validar.php',
            data: {
                usr: usuario,
                pass: password
            },
            dataType: 'json',
            type: 'get',
            success: function(data){
                if (data.validacion) {
                    $('#usrId').val(data.id);
                    console.log(data.id);
                    $('#formLogin').unbind('submit').submit();
                }
            }
        })
    })
})